#pragma once

#ifndef RNG_LONGDIV_PIE
#define RNG_LONGDIV_PIE


class PIE_RNG
{
	private:
		//Two numbers used to calculate Pie
		int numA = 355;
		int numB = 113;


	public:
		PIE_RNG()
		{

		}

		int generateNumber(double low, double high);
		int randomInt();

};


#endif