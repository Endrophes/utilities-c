#include "pch.h"
#include "PIE_RNG.h"

#include <iostream>
using namespace std;

#define INT_BITS (sizeof(int)*8)

int PIE_RNG::generateNumber(double low, double high)
{
	int dividend = PIE_RNG::numA;
	int divisor  = PIE_RNG::numB;

	int quotient = 0;
	int remainder = 0;

	for (int i = 0; i < INT_BITS; i++)
	{
		quotient  <<= 1;
		remainder <<= 1;

		remainder += (dividend >> (INT_BITS - 1)) & 1;
		dividend  <<= 1;

		if (remainder >= divisor)
		{
			remainder -= divisor;
			quotient++;
		}

		cout << "quotient == "  << quotient;
		cout << "remainder == " << remainder;

	}

	return 0;
}

int main()
{
	PIE_RNG pie_Gen;
	pie_Gen.generateNumber(0.0, 5.0);
}
